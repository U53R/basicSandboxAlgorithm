import java.awt.Color;
import java.awt.Graphics2D;

public class Background {
	private Color color;
public Background () {
	color = Color.BLUE;
}
public void draw(Graphics2D g) {
	g.setColor(color);
	g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
}
}
