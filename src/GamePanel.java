import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;

public class GamePanel extends JPanel implements Runnable {
	
	public static int WIDTH = 1500;
	public static int HEIGHT = 1000;
	
private Thread thread;
	
	private BufferedImage image;
	
	private Graphics2D g;
	
	private int FPS;
	private double millisToFPS;
	private long timerFPS;
	private int sleepTime;
	

	
	//Classes
	private Background bg;
	private Player player;
	
	//ArrayLists
	private ArrayList<ArrayList<Block>> blocks;
	
	//Booleans
	private boolean falling = true;
	public static boolean left, right,building = false;
	private boolean able;
	
	//Ints
	public static int jumpCount = 0;
	public int mouseX, mouseY;
	private int buildX;
	
	//Blocks
	private int blockSize = 20;
	private int blockPosY = 800;
	private int blockPosX = -300;
	private int minBlockY = 180;
	private int maxBlockY = 210;
	private int blocksX = 500;
	
	public GamePanel() {
super();
		
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setFocusable(true);
		requestFocus();
		
		addKeyListener(new Listeners(this));
		addMouseMotionListener(new Listeners(this));
		addMouseListener(new Listeners(this));
		
		bg = new Background();
		blocks = new ArrayList<ArrayList<Block>>();
		player = new Player(WIDTH/2,600,20,60,WIDTH/2+blockPosX*-1);
		generate();
	}

	@Override
	public void run() {
		FPS = 30;
		millisToFPS = 1000/FPS;
		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) image.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		while (true) {
			timerFPS = System.nanoTime();
			gameUpdate();
			gameRender();
			gameDraw();
			timerFPS = (System.nanoTime()-timerFPS)/1000000;
			if (millisToFPS > timerFPS) {
				sleepTime = (int) (millisToFPS - timerFPS);
			}
			else {
				sleepTime = 1;
			}
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void start() {
		thread = new Thread(this);
		thread.start();
	}
	
	public void gameUpdate() {
		//building
		if (building) {
			showBuild();
		}
		//falling
		if (jumpCount == 0) {
		if (player.getPosX()%20 != 0) {
			for (int i = 0; i < 2; i++) {
				for (int a = blocks.get(player.getPosX()/20+i).size()-1; a > -1; a--) {
					if (blocks.get(player.getPosX()/20+i).get(a).getY() == player.getY() + player.getH()) {
						falling = false;
						break;
					}
				}
			}
		}
		else {
			for (int i = blocks.get(player.getPosX()/20).size()-1; i > -1; i--) {
				if (blocks.get(player.getPosX()/20).get(i).getY() == player.getY() + player.getH()) {
					falling = false;
					break;
				}
			}
		}
		if (falling) {
			for (int i = 0; i < blocks.size(); i++) {
				for (int a = 0; a < blocks.get(i).size(); a++) {
					blocks.get(i).get(a).setY(blocks.get(i).get(a).getY()-10);
				}
			}
		}
		else {
			falling = true;
		}
		}
		
		//jumping
		else {
			boolean rise = true;
			if (player.getY()%20 == 0) {
		int pos = player.getPosX();
		if (pos%20 != 0) {
			pos/=20;
			for (int i = 0; i < 2; i++) {
				if (rise) {
				for (int a = 0; a < blocks.get(pos+i).size(); a++) {
					if (blocks.get(pos+i).get(a).getY()+blocks.get(pos+i).get(a).getS() == player.getY()) {
						rise = false;
						break;
					}
				}
				}
			}
		}
		else {
			pos/=20;
			for (int i = 0; i < blocks.get(pos).size(); i++) {
				if (blocks.get(pos).get(i).getY() + blocks.get(pos).get(i).getS() == player.getY()) {
					rise = false;
					break;
				}
			}
		}
			}
			if (rise) {
				for (int i = 0; i < blocks.size(); i++) {
					for (int a = 0; a < blocks.get(i).size(); a++) {
						blocks.get(i).get(a).setY(blocks.get(i).get(a).getY()+10);
					}
				}
			}
			jumpCount--;
		}
		
		///moving
		if (left) {
			boolean go = true;
			if (player.getPosX()%20 == 0) {
				int a = 0;
				while (blocks.get(player.getPosX()/20-1).get(a).getY() < player.getY() + player.getH()) {
					if ((blocks.get(player.getPosX()/20-1).get(a).getY() + blocks.get(player.getPosX()/20-1).get(a).getS() > player.getY()) ) {
						go = false;
						break;
					}
					a++;
				}
			}
			if (go) {
				player.setPosX(player.getPosX()-10);
				for (int i = 0; i < blocks.size(); i++) {
					for (int a = 0; a < blocks.get(i).size(); a++) {
						blocks.get(i).get(a).setX(blocks.get(i).get(a).getX()+10);
					}
				}
			}
		}
		if (right) {
			boolean go = true;
			if (player.getPosX()%20 == 0) {
				int a = 0;
				while (blocks.get(player.getPosX()/20+1).get(a).getY() < player.getY() + player.getH()) {
					if ((blocks.get(player.getPosX()/20+1).get(a).getY() + blocks.get(player.getPosX()/20+1).get(a).getS() > player.getY()) ) {
						go = false;
						break;
					}
					a++;
				}
			}
			if (go) {
				player.setPosX(player.getPosX()+10);
				for (int i = 0; i < blocks.size(); i++) {
					for (int a = 0; a < blocks.get(i).size(); a++) {
						blocks.get(i).get(a).setX(blocks.get(i).get(a).getX()-10);
					}
				}
			}
		}
		
	}
	
	public void gameRender() {
		bg.draw(g);
		for (int i = 0; i < blocks.size(); i++) {
			for (int a = 0; a < blocks.get(i).size(); a++) {
				if (blocks.get(i).get(a).getX() + blocks.get(i).get(a).getS() >= 0 
						&& blocks.get(i).get(a).getY() + blocks.get(i).get(a).getS() >= 0
						&& blocks.get(i).get(a).getX() <= WIDTH
						&& blocks.get(i).get(a).getY() <= HEIGHT) {
					blocks.get(i).get(a).draw(g);
				}
			}
		}
		player.draw(g);
		if (able && building) {
			g.setColor(Color.CYAN);
			g.fillRect(blocks.get(buildX).get(0).getX(), mouseY/20*20, blockSize, blockSize);
		}
	}

	public void gameDraw() {
		Graphics g2 = this.getGraphics();
		g2.drawImage(image, 0, 0, null);
		g2.dispose();
	}
	
	public void build() {
		if (able) {
			int i = 0;
			while (blocks.get(buildX).get(i).getY() < mouseY) {
				i++;
			}
			blocks.get(buildX).add(i,new Block(blocks.get(buildX).get(0).getX(), mouseY/20*20, blockSize, Color.GRAY));
		}
	}
	
	private void showBuild() {
		able = true;
		int pos = 0;
		if (Math.abs((player.getY() + player.getH()/2) - mouseY) <= 69 && Math.abs((player.getX()+player.getW()/2)-mouseX) <= 49
				&& (Math.abs((player.getY() + player.getH()/2)-mouseY) >= 30 || Math.abs((player.getX()+player.getW()/2)-mouseX) >= 10 && jumpCount == 0)) {
			double a = player.getX()+player.getW()/2 - mouseX;
			a =  Math.round(a/20);
		
			a*=-1;
			 pos = player.getPosX()/20+(int)a;
			if (player.getPosX()%20 != 0 && !(mouseX < player.getX())) {
				pos++;
			}
			int i = 0;
			while (blocks.get(pos).get(i).getY() < mouseY) {
				if (blocks.get(pos).get(i).getY() <= mouseY && blocks.get(pos).get(i).getY() + blocks.get(pos).get(i).getS() >= mouseY) {
					able = false;
					break;
				}
				i++;
			}
		}
		else {
			able = false;
		}
		buildX = pos;
	}
	
	private void generate() {
		int h = (int) (Math.random()*maxBlockY);
		if (h < minBlockY) {
			h = minBlockY;
		}
		int x = blockPosX;
		int prevh = h;
		int y = blockPosY + ((h-minBlockY)*blockSize);
		int strike = 5;
		int strikeC = 0;
		
		for (int i = 0; i < blocksX; i++) {
			blocks.add(new ArrayList<Block>());
			
			for (int a = 0; a < h; a++) {
				if (a == 0) {
				blocks.get(i).add(new Block(x, y, blockSize, Color.GREEN));
				}
				else if (a > 0 && a < 100) {
					blocks.get(i).add(new Block(x, y, blockSize, Color.GRAY));
				}
				else {
					blocks.get(i).add(new Block(x, y, blockSize, Color.DARK_GRAY));
				}
				y+=blockSize;
			}
			int diff = (int) (Math.random()*5);
			if (Math.random()*50 >= 24) {
				diff*=-1;
			}
			if (diff != 0 && strikeC == 0) {
				strike--;
			}
			if (strike == 0) {
				strike = 5;
				strikeC = 5;
			}
			if (strikeC > 0) {
				strikeC--;
				diff = 0;
			}
			y = blockPosY + ((prevh + diff)-minBlockY)*blockSize;
			prevh = prevh + diff;
			x+=blockSize;
			if (prevh < minBlockY) {
				y = blockPosY;
				prevh = minBlockY;
			}
		}
	}
	
	public void hit(int x, int y) {
		if (Math.abs((player.getY() + player.getH()/2) - y) <= 69 && Math.abs((player.getX()+player.getW()/2)-x) <= 49) {
			double a = player.getX()+player.getW()/2 - x;
			a =  Math.round(a/20);
		
			a*=-1;
			int pos = player.getPosX()/20+(int)a;
			if (player.getPosX()%20 != 0) {
				pos++;
			}
			for (int i = 0; i < blocks.get(pos).size(); i++) {
				if (blocks.get(pos).get(i).getY() <= y && blocks.get(pos).get(i).getY() + blocks.get(pos).get(i).getS() >= y) {
					blocks.get(pos).remove(i);
					break;
				}
			}
		}
	}
	
	
	
}

