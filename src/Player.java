import java.awt.Color;
import java.awt.Graphics2D;

public class Player {
private int x,y,w,h,posX;
private Color color;
public Player(int x, int y, int w, int h, int posX) {
	color = Color.RED;
	this.x = x;
	this.y = y;
	this.w = w;
	this.h= h;
	this.posX = posX;
}
public int getX() {
	return x;
}
public int getY() {
	return y;
}
public int getW() {
	return w;
}
public int getH() {
	return h;
}
public int getPosX() {
	return posX;
}
public void setPosX(int posX) {
	this.posX = posX;
}
public void draw(Graphics2D g) {
	g.setColor(color);
	g.fillRect(x, y, w, h);
}
}
