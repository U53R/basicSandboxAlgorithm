import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class Listeners implements KeyListener, MouseListener, MouseMotionListener{
	GamePanel gmp;
	
	public Listeners (GamePanel gmp) {
		this.gmp = gmp;
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (gmp.building) {
			gmp.mouseX = e.getX();
			gmp.mouseY = e.getY();
		}
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (gmp.building) {
			gmp.build();
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		if (!gmp.building) {
		gmp.hit(x, y);
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		if (key == KeyEvent.VK_D) {
			GamePanel.right = true;
		}
		
		if (key == KeyEvent.VK_A) {
			GamePanel.left = true;
		}
		
		if (key == KeyEvent.VK_SPACE && GamePanel.jumpCount == 0) {
			GamePanel.jumpCount = 8;
		}
		
		if (key == KeyEvent.VK_1) {
			if (!gmp.building) {
				gmp.building = true;
			}
			else {
				gmp.building = false;
			}
		}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
int key = e.getKeyCode();
		
		if (key == KeyEvent.VK_D) {
			GamePanel.right = false;
		}
		
		if (key == KeyEvent.VK_A) {
			GamePanel.left = false;
		}
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
		
		
	}

}
