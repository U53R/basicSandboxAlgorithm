import java.awt.Color;
import java.awt.Graphics2D;

public class Block {
private int x,y,s;
private Color color;
public Block(int x, int y, int s, Color color) {
	this.x = x;
	this.y = y;
	this.s = s;
	this.color = color;
}
public int getX() {
	return x;
}
public int getY() {
	return y;
}
public int getS() {
	return s;
}
public void setY(int y) {
	this.y = y;
}
public void setX(int x) {
	this.x = x;
}
public void draw(Graphics2D g) {
	g.setColor(color);
	g.fillRect(x, y, s, s);
}
}
